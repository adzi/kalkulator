﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.result = new System.Windows.Forms.TextBox();
            this.jedan = new System.Windows.Forms.Button();
            this.dva = new System.Windows.Forms.Button();
            this.sest = new System.Windows.Forms.Button();
            this.sedam = new System.Windows.Forms.Button();
            this.osam = new System.Windows.Forms.Button();
            this.tri = new System.Windows.Forms.Button();
            this.puta = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.djeli = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.cetri = new System.Windows.Forms.Button();
            this.devet = new System.Windows.Forms.Button();
            this.jednako = new System.Windows.Forms.Button();
            this.nula = new System.Windows.Forms.Button();
            this.pet = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.dec = new System.Windows.Forms.Button();
            this.ispis = new System.Windows.Forms.Label();
            this.hist = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // result
            // 
            this.result.Font = new System.Drawing.Font("AR JULIAN", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.result.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.result.Location = new System.Drawing.Point(14, 11);
            this.result.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(337, 26);
            this.result.TabIndex = 0;
            this.result.Text = "0.0";
            this.result.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.result.TextChanged += new System.EventHandler(this.result_TextChanged);
            // 
            // jedan
            // 
            this.jedan.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jedan.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.jedan.Location = new System.Drawing.Point(27, 224);
            this.jedan.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.jedan.Name = "jedan";
            this.jedan.Size = new System.Drawing.Size(87, 34);
            this.jedan.TabIndex = 1;
            this.jedan.Text = "1";
            this.jedan.UseVisualStyleBackColor = true;
            this.jedan.Click += new System.EventHandler(this.button_Click);
            // 
            // dva
            // 
            this.dva.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dva.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dva.Location = new System.Drawing.Point(131, 224);
            this.dva.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.dva.Name = "dva";
            this.dva.Size = new System.Drawing.Size(87, 34);
            this.dva.TabIndex = 2;
            this.dva.Text = "2";
            this.dva.UseVisualStyleBackColor = true;
            this.dva.Click += new System.EventHandler(this.button_Click);
            // 
            // sest
            // 
            this.sest.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sest.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.sest.Location = new System.Drawing.Point(234, 152);
            this.sest.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.sest.Name = "sest";
            this.sest.Size = new System.Drawing.Size(87, 34);
            this.sest.TabIndex = 3;
            this.sest.Text = "6";
            this.sest.UseVisualStyleBackColor = true;
            this.sest.Click += new System.EventHandler(this.button_Click);
            // 
            // sedam
            // 
            this.sedam.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sedam.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.sedam.Location = new System.Drawing.Point(27, 67);
            this.sedam.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.sedam.Name = "sedam";
            this.sedam.Size = new System.Drawing.Size(87, 34);
            this.sedam.TabIndex = 4;
            this.sedam.Text = "7";
            this.sedam.UseVisualStyleBackColor = true;
            this.sedam.Click += new System.EventHandler(this.button_Click);
            // 
            // osam
            // 
            this.osam.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.osam.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.osam.Location = new System.Drawing.Point(131, 67);
            this.osam.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.osam.Name = "osam";
            this.osam.Size = new System.Drawing.Size(87, 34);
            this.osam.TabIndex = 5;
            this.osam.Text = "8";
            this.osam.UseVisualStyleBackColor = true;
            this.osam.Click += new System.EventHandler(this.button_Click);
            // 
            // tri
            // 
            this.tri.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tri.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tri.Location = new System.Drawing.Point(234, 224);
            this.tri.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.tri.Name = "tri";
            this.tri.Size = new System.Drawing.Size(87, 34);
            this.tri.TabIndex = 6;
            this.tri.Text = "3";
            this.tri.UseVisualStyleBackColor = true;
            this.tri.Click += new System.EventHandler(this.button_Click);
            // 
            // puta
            // 
            this.puta.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.puta.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.puta.Location = new System.Drawing.Point(361, 224);
            this.puta.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.puta.Name = "puta";
            this.puta.Size = new System.Drawing.Size(87, 34);
            this.puta.TabIndex = 7;
            this.puta.Text = "*";
            this.puta.UseVisualStyleBackColor = true;
            this.puta.Click += new System.EventHandler(this.operator_click);
            // 
            // minus
            // 
            this.minus.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minus.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.minus.Location = new System.Drawing.Point(361, 163);
            this.minus.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(87, 34);
            this.minus.TabIndex = 8;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.operator_click);
            // 
            // djeli
            // 
            this.djeli.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.djeli.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.djeli.Location = new System.Drawing.Point(361, 7);
            this.djeli.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.djeli.Name = "djeli";
            this.djeli.Size = new System.Drawing.Size(87, 34);
            this.djeli.TabIndex = 9;
            this.djeli.Text = "/";
            this.djeli.UseVisualStyleBackColor = true;
            this.djeli.Click += new System.EventHandler(this.operator_click);
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button10.Location = new System.Drawing.Point(361, 278);
            this.button10.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(87, 34);
            this.button10.TabIndex = 10;
            this.button10.Text = "ce";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button11.Location = new System.Drawing.Point(358, 109);
            this.button11.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(90, 34);
            this.button11.TabIndex = 11;
            this.button11.Text = "c";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // cetri
            // 
            this.cetri.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cetri.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cetri.Location = new System.Drawing.Point(27, 152);
            this.cetri.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.cetri.Name = "cetri";
            this.cetri.Size = new System.Drawing.Size(87, 34);
            this.cetri.TabIndex = 13;
            this.cetri.Text = "4";
            this.cetri.UseVisualStyleBackColor = true;
            this.cetri.Click += new System.EventHandler(this.button_Click);
            // 
            // devet
            // 
            this.devet.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.devet.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.devet.Location = new System.Drawing.Point(234, 67);
            this.devet.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.devet.Name = "devet";
            this.devet.Size = new System.Drawing.Size(87, 34);
            this.devet.TabIndex = 14;
            this.devet.Text = "9";
            this.devet.UseVisualStyleBackColor = true;
            this.devet.Click += new System.EventHandler(this.button_Click);
            // 
            // jednako
            // 
            this.jednako.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jednako.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.jednako.Location = new System.Drawing.Point(459, 18);
            this.jednako.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.jednako.Name = "jednako";
            this.jednako.Size = new System.Drawing.Size(90, 293);
            this.jednako.TabIndex = 15;
            this.jednako.Text = "=";
            this.jednako.UseVisualStyleBackColor = true;
            this.jednako.Click += new System.EventHandler(this.button15_Click);
            // 
            // nula
            // 
            this.nula.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nula.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.nula.Location = new System.Drawing.Point(27, 278);
            this.nula.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.nula.Name = "nula";
            this.nula.Size = new System.Drawing.Size(191, 34);
            this.nula.TabIndex = 16;
            this.nula.Text = "0";
            this.nula.UseVisualStyleBackColor = true;
            this.nula.Click += new System.EventHandler(this.button_Click);
            // 
            // pet
            // 
            this.pet.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pet.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pet.Location = new System.Drawing.Point(131, 152);
            this.pet.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.pet.Name = "pet";
            this.pet.Size = new System.Drawing.Size(87, 34);
            this.pet.TabIndex = 17;
            this.pet.Text = "5";
            this.pet.UseVisualStyleBackColor = true;
            this.pet.Click += new System.EventHandler(this.button_Click);
            // 
            // plus
            // 
            this.plus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.plus.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plus.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.plus.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.plus.Location = new System.Drawing.Point(361, 67);
            this.plus.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(87, 34);
            this.plus.TabIndex = 18;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.operator_click);
            // 
            // dec
            // 
            this.dec.Font = new System.Drawing.Font("Arial Black", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dec.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dec.Location = new System.Drawing.Point(234, 278);
            this.dec.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.dec.Name = "dec";
            this.dec.Size = new System.Drawing.Size(87, 34);
            this.dec.TabIndex = 19;
            this.dec.Text = ".";
            this.dec.UseVisualStyleBackColor = true;
            this.dec.Click += new System.EventHandler(this.button_Click);
            // 
            // ispis
            // 
            this.ispis.AutoSize = true;
            this.ispis.BackColor = System.Drawing.SystemColors.Info;
            this.ispis.Font = new System.Drawing.Font("AR JULIAN", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ispis.Location = new System.Drawing.Point(39, 18);
            this.ispis.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.ispis.Name = "ispis";
            this.ispis.Size = new System.Drawing.Size(0, 14);
            this.ispis.TabIndex = 20;
            // 
            // textBox1
            // 
            this.hist.Font = new System.Drawing.Font("AR JULIAN", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hist.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.hist.Location = new System.Drawing.Point(14, 109);
            this.hist.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.hist.Name = "textBox1";
            this.hist.Size = new System.Drawing.Size(337, 26);
            this.hist.TabIndex = 21;
            this.hist.Text = "0.0";
            this.hist.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(557, 330);
            this.Controls.Add(this.hist);
            this.Controls.Add(this.ispis);
            this.Controls.Add(this.dec);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.pet);
            this.Controls.Add(this.nula);
            this.Controls.Add(this.jednako);
            this.Controls.Add(this.devet);
            this.Controls.Add(this.cetri);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.djeli);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.puta);
            this.Controls.Add(this.tri);
            this.Controls.Add(this.osam);
            this.Controls.Add(this.sedam);
            this.Controls.Add(this.sest);
            this.Controls.Add(this.dva);
            this.Controls.Add(this.jedan);
            this.Controls.Add(this.result);
            this.Font = new System.Drawing.Font("AR JULIAN", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(773, 564);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(573, 364);
            this.Name = "Form1";
            this.Text = "kalkulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox result;
        private System.Windows.Forms.Button jedan;
        private System.Windows.Forms.Button dva;
        private System.Windows.Forms.Button sest;
        private System.Windows.Forms.Button sedam;
        private System.Windows.Forms.Button osam;
        private System.Windows.Forms.Button tri;
        private System.Windows.Forms.Button puta;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button djeli;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button cetri;
        private System.Windows.Forms.Button devet;
        private System.Windows.Forms.Button jednako;
        private System.Windows.Forms.Button nula;
        private System.Windows.Forms.Button pet;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button dec;
        private System.Windows.Forms.Label ispis;
        private System.Windows.Forms.TextBox hist;
       
    }
}

